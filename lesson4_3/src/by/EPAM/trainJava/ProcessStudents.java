package by.EPAM.trainJava;

/***
 * ������ 4. ������� 3
 * �) ������� ���� ������� ������ (������������ ����� Group);
 * �) ������� ���� ������� ��������;
 * �) ����� ����������;
 * �) ���������� ���������, ������� "2"
*/
public class ProcessStudents {
	/***
	 * ������ ���������
	 */
	private Student [] arSt;
	/***
	 * ������ �����
	 */
	private Group [] arGr;
	/***
	 * �������� ���������. 
	 * �������� �����, ��������, ������������� �� �������, ������ ���� �����������.
	 * !!! ���� ��������� ����� ������ ��� ���� � �������, �� ������ ���������� �� ����� � "������"
	 * �������� ������ �� ����� ���������������. !!! 
	 */
	void process (){
		// -------------------  �������� 3 ������ (id 2,3,4) � 18 ���������
		arGr = new Group[3];
		arSt = new Student [18];
		
		for (int i = 0; i < arGr.length; i++) {
			arGr[i]=new Group(i+2,"������ "+i+2,7);
		} 
		
		for (int i = 0; i < arSt.length; i++) {
			// ���������� ����������� ��� �������� ���������
			arSt[i] = new Student ((i+1));
			// ������������ ��������� �� �������
			int g=0;
			for (int j = 0; j < arGr.length; j++) {
				// ���� ����� � ������ ���, ����� ��������� � ���������.
				if (arGr[j].addStudent(arSt[i]) ) {
					// ��������
					break;
				}
			}
		}
		// -------------------  
		int totalOtl=0;
		int totalDvn=0;
		// ���������� �� �������
		for (int j = 0; j < arGr.length; j++) {
			System.out.println("__________________");
			// ����� �������� �����, ������� ������.
			arGr[j].printGroup();
			System.out.println("�� ���������:");
			int otlQnt=0;
			int dvnQnt=0;
			double avgM=0;
			for (int i=0; i<arGr[j].getCapacity(); i++) {
				Student ss=arGr[j].getStudent(i);
				if (ss!=null) {
					avgM=ss.getAvgMark();
					System.out.println(ss.getAllInfo());
					if (avgM==5) {
						otlQnt++;
					}
					if (dvnQnt<3) {
						dvnQnt++;
					}
				}	
			}
			System.out.println("���������� ����������: "+otlQnt);
			System.out.println("���������� ����������: "+dvnQnt);
			totalOtl+=otlQnt;
			totalDvn+=dvnQnt;
		}
		System.out.println("__________________");
		System.out.println("����� ����� - "+arGr.length+". � ��� ");
		System.out.println("���������� ����������: "+totalOtl);
		System.out.println("���������� ����������: "+totalDvn);
	}
	
	public static void main(String[] args) {
		ProcessStudents p =new ProcessStudents();
		p.process();
	}
}
