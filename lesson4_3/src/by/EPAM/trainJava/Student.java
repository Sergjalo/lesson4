package by.EPAM.trainJava;

import java.util.concurrent.ThreadLocalRandom;
/***
 *  
 * @author Sergii_Kotov
 * ������ 4. ������� 3
 * ����� ��� �������� �������� � ��� ������.
 * ����� �������� � ������ - � ������������ �� ����� ������.
 */
public class Student {
	/***
	 * ���������� ��������� �� ������� ������������ ������
	 */
	public static int numCourses =4;
	/***
	 * ��� �������� 
	 */
	private int id; 
	/***
	 * ���
	 */
	private String surname, name, secName;
	/***
	 * ������ ������
	 */
	private int [] marks = new int [numCourses];
		
	/***
	 * ����������� �� ���������, ����� ���-�� �������
	 */
	public Student () {
		this(1,"Petrov", "Petr", "Lukich");
	}
			
	/***
	 * �������� �����������
	 * @param i ��� ��������
	 * @param sr �������
	 * @param sn ���
	 * @param snn ��������
	 */
	public Student (int i, String sr, String sn, String snn) {
		id =i;
		surname =sr; name=sn; secName=snn;
	}

	/***
	 * ��� �������� ��������� ���������
	 * @param i	��� ��������
	 * @param igr ����� 
	 */
	public Student (int i) {
		id =i;
		surname ="Surname_"+i; name="Name_"+i; secName="Secname_"+i;
		for (int j = 0; j < marks.length; j++) {
			marks[j]=ThreadLocalRandom.current().nextInt(2, 10); // ����� ���� �������� 5
			if (marks[j]>5) {
				marks[j]=5;
			}
		}
	}
	
	/***
	 * �� ������� ������ marks ������� ������� ������ 
	 * @return
	 */
	public double getAvgMark (){
		double t=0;
		for (int j = 0; j < marks.length; j++) {
			t+=marks[j];
		}
		return Math.round(t/marks.length*10)/10;
	}
	
	/***
	 * ����� ���������� � ��������
	 * @return ������ � ��� � ������� ������
	 */
	public String getAllInfo() {
		return "id="+id+ " ���="+surname+ " "+name+" "+secName+ ". ������� ����= "+ Math.round(getAvgMark()*10)/10;
	}
	
	public void setId(int i) {
		id = i;
	}
	
	public void setSurname (String s) {
		surname=s;
	}

	public void setName (String s) {
		name=s;
	}
	
	public void setSecName (String s) {
		secName=s;
	}

	public int getId() {
		return id;
	}
		
	public String getSurname() {
		return surname;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSecName() {
		return secName;
	}

}
