package by.EPAM.trainJava;

import java.util.Scanner;

/***
 * 
 * @author Sergii_Kotov
 * ������ 4. ������� 2
 * ������������� Fraction
 */
public class FractArray {

	private static final int k=5;
	private static Fraction[] arFr = new Fraction [k];
	static Scanner scanner = new Scanner(System.in);
	
	// ����� �������� ���������
	public static void main(String[] args) {
		for (int i = 0; i < arFr.length-2; i++) {
			arFr[i]= new  Fraction ();
		}
		// ����� �������� ������
		arFr[arFr.length-2]= enterValues();
		arFr[arFr.length-1]= enterValues();
		
		for (int i = 0; i < arFr.length; i++) {
			arFr[i].printFraction();
			System.out.println();
		}

	}
	
	static private Fraction enterValues (){
		int m=1, n=1;
		System.out.println("������� ���������");
		if(scanner.hasNextInt()) {
			m= scanner.nextInt();
		}else{
			scanner.next();
		}
		System.out.println("������� �����������");
		if(scanner.hasNextInt()) {
			n= scanner.nextInt();
		}else{
			scanner.next();
		}
		return new Fraction (m,n);
	}
	
}

