package by.EPAM.trainJava;

import java.util.Calendar;
import java.util.Date;

/***
 * 
 * @author Sergii_Kotov
 * ������ 4. ������� 1 
 */
// Customer: id, �������, ���, ��������, �����, ����� ��������� ��������, ����� ����������� �����.
public class Customer {
	private int id;
	private String surname, name, secName;
	private String addr, credNumber, accNumber;

	public Customer () {
		this(0,"","","","","","");		
	}

	public Customer (int i, String sr, String sn, String snn,
		String adr, String cred, String acc) {
		id =i;
		surname =sr; name=sn; secName=snn;
		addr=adr;  credNumber=cred;  accNumber=acc; 
	}
	
	public void show (int whatToShow){
		switch (whatToShow) {
		case 0: 
			System.out.println("���: "+surname + " " + name + " " + secName);
			break;
		case 1:
			System.out.println("����� ��������: "+credNumber);
			break;
		case 2:
			System.out.println("������ ������: "+surname + " " + name + " " + secName);
			System.out.println("�����: "+ addr);
			break;
		default: System.out.println("���: "+surname + " " + name + " " + secName);
				 System.out.println("����� ��������: "+credNumber);
				 System.out.println("�����: "+ addr);
		}
		 System.out.println(" ��������� ���������: "+getDepartment());
	}
	
	public String getDepartment (){
		return "������� ��� �� �����������";
	}
	
	public void setId (int i) {
		id=i;
	}

	public void setSurname (String s) {
		surname=s;
	}

	public void setName (String s) {
		name=s;
	}
	
	public void setSecName (String s) {
		secName=s;
	}
	
	public void setCredNumber (String s) {
		credNumber=s;
	}
	
	public void setAccNumber(String s) {
		accNumber=s;
	}
		
	public int getId() {
		return id;
	}
		
	public String getSurname() {
		return surname;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSecName() {
		return secName;
	}
	
	public String getAccNumber() {
		return accNumber;
	}
	
	public String getCredNumber() {
		return credNumber;
	}
	
	public String getAddr() {
		return addr;
	}
		

}
