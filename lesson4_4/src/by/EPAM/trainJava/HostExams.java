package by.EPAM.trainJava;

import java.util.Arrays;

/***
 * 
 * @author Sergii_Kotov
 * ������ 4. ������� 4
 * ������� ������ ������������ (����� Abiturient) � ������ ������, 
 * ���������� ��� �� ������������� ���������. 
 * ���������� ������ �����������, ���� ����� ���� ������ ����� ������������.
 */
public class HostExams {
	/***
	 * ���������� ������� ����
	 */
	int quantityOfStudyPlaces;
	/***
	 * ������ ������������
	 */
	private Abiturient [] arA;
	/***
	 * ���������� ������������
	 */
	private final int numAbitur = 18;
	/***
	 * ���������� ��������� ����
	 */
	private final int numFree = 15;
	
	void process (){
		// ���� ������� - ����� ������ ������� ���� �� ���������� ��������� ����.
		
		arA = new Abiturient [numAbitur];
		for (int i = 0; i < arA.length; i++) {
			// ���������� ����������� ��� �������� ���������
			arA[i] = new Abiturient ((i+1));
			//System.out.println(arA[i].printAllInfo());
		}
		// ��������� �� ��������
		Arrays.sort(arA);
		// �� ������ ������� ���-�� ��������� ��� ����. ����
		int realNumStudents = Math.min (numAbitur, numFree); 
		System.out.println("��������� ������ � �������: ");
		int i;
		for (i = 0; i < realNumStudents; i++) {
			System.out.println(arA[i].printAllInfo());
		}
		if (i<numAbitur) {
			System.out.println("�� ���������: ");
			for (int j = i; j < numAbitur; j++) {
				System.out.println(arA[j].printAllInfo());
			}
		}
	}
	
	public static void main(String[] args) {
		HostExams p =new HostExams();
		p.process();
	}
	
}
